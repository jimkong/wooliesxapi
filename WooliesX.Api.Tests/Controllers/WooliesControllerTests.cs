﻿using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Api.Controllers;
using WooliesX.Api.Enums;
using WooliesX.Api.Models;
using WooliesX.Api.Services;

namespace WooliesX.Api.Tests.Controllers
{
    [TestFixture]
    public class WooliesControllerTests
    {
        private IWooliesService _wooliesService;
        private WooliesController _wooliesController;

        [SetUp]
        public void Setup()
        {
            _wooliesService = Substitute.For<IWooliesService>();

            _wooliesController = new WooliesController(_wooliesService);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnBadRequest_IfInvalidSortOption()
        {
            var response = await _wooliesController.GetSortedProducts("foobar");

            Assert.IsInstanceOf(typeof(BadRequestResult), response.Result);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnOk_IfValidSortOption()
        {
            _wooliesService.GetSortedProducts(Arg.Any<SortOption>()).Returns(new List<Product>());

            var response = await _wooliesController.GetSortedProducts("low");

            Assert.IsInstanceOf(typeof(OkObjectResult), response.Result);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnOk_IfValidSortOptionIgnoringCase()
        {
            _wooliesService.GetSortedProducts(Arg.Any<SortOption>()).Returns(new List<Product>());

            var response = await _wooliesController.GetSortedProducts("HIGH");

            Assert.IsInstanceOf(typeof(OkObjectResult), response.Result);
        }
    }
}
