﻿using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Api.Enums;
using WooliesX.Api.Models;
using WooliesX.Api.Services;

namespace WooliesX.Api.Tests.Services
{
    [TestFixture]
    public class WooliesServiceTests
    {
        private IOptions<User> _configuration;
        private IResourceService _resourceService;
        private WooliesService _wooliesService;

        [SetUp]
        public void Setup()
        {
            _configuration = Substitute.For<IOptions<User>>();
            _configuration.Value.Returns(new User
            {
                Name = "foobar",
                Token = "foobar-123"
            });

            _resourceService = Substitute.For<IResourceService>();
            _resourceService.GetProducts().Returns(new List<Product>
            {
                new Product
                {
                    Name = "foo",
                    Price = 15,
                    Quantity = 0
                },
                new Product
                {
                    Name = "bar",
                    Price = 10,
                    Quantity = 0
                },
                new Product
                {
                    Name = "foobar",
                    Price = 5,
                    Quantity = 0
                },
            });
            _wooliesService = new WooliesService(_configuration, _resourceService);
        }

        [Test]
        public void GetUserDetails_ShouldReturnUserDetails()
        {
            var details = _wooliesService.GetUserDetails();

            Assert.AreEqual("foobar", details.Name);
            Assert.AreEqual("foobar-123", details.Token);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnSortedLowToHighPrice_IfLowOption()
        {
            var products = (await _wooliesService.GetSortedProducts(SortOption.Low)).ToList();

            Assert.AreEqual("foobar", products[0].Name);
            Assert.AreEqual("bar", products[1].Name);
            Assert.AreEqual("foo", products[2].Name);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnSortedHighToLowPrice_IfHighOption()
        {
            var products = (await _wooliesService.GetSortedProducts(SortOption.High)).ToList();

            Assert.AreEqual("foobar", products[2].Name);
            Assert.AreEqual("bar", products[1].Name);
            Assert.AreEqual("foo", products[0].Name);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnSortedAscendingName_IfAscendingOption()
        {
            var products = (await _wooliesService.GetSortedProducts(SortOption.Ascending)).ToList();

            Assert.AreEqual("bar", products[0].Name);
            Assert.AreEqual("foo", products[1].Name);
            Assert.AreEqual("foobar", products[2].Name);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnSortedDescendingName_IfDescendingOption()
        {
            var products = (await _wooliesService.GetSortedProducts(SortOption.Descending)).ToList();

            Assert.AreEqual("bar", products[2].Name);
            Assert.AreEqual("foo", products[1].Name);
            Assert.AreEqual("foobar", products[0].Name);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnMostPopular_IfRecommendedOption()
        {
            _resourceService.GetShopperHistory().Returns(new List<ShopperHistory>
            {
                new ShopperHistory
                {
                    CustomerId = 1,
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Name = "foo",
                            Price = 15,
                            Quantity = 3
                        },
                        new Product
                        {
                            Name = "foobar",
                            Price = 5,
                            Quantity = 2
                        }
                    }
                },
                new ShopperHistory
                {
                    CustomerId = 2,
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Name = "foo",
                            Price = 15,
                            Quantity = 1
                        }
                    }
                }
            });

            var products = (await _wooliesService.GetSortedProducts(SortOption.Recommended)).ToList();

            Assert.AreEqual("foo", products[0].Name);
            Assert.AreEqual("foobar", products[1].Name);
            Assert.AreEqual("bar", products[2].Name);
        }

        [Test]
        public async Task GetSortedProducts_ShouldReturnMostPopular_IfExtraProductsInShopperHistory()
        {
            _resourceService.GetShopperHistory().Returns(new List<ShopperHistory>
            {
                new ShopperHistory
                {
                    CustomerId = 1,
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Name = "foo",
                            Price = 15,
                            Quantity = 3
                        },
                        new Product
                        {
                            Name = "foobar",
                            Price = 5,
                            Quantity = 2
                        }
                    }
                },
                new ShopperHistory
                {
                    CustomerId = 2,
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Name = "foo",
                            Price = 15,
                            Quantity = 1
                        }
                    }
                },
                new ShopperHistory
                {
                    CustomerId = 3,
                    Products = new List<Product>
                    {
                        new Product
                        {
                            Name = "barfoo",
                            Price = 15,
                            Quantity = 1
                        }
                    }
                }
            });

            var products = (await _wooliesService.GetSortedProducts(SortOption.Recommended)).ToList();

            Assert.AreEqual("foo", products[0].Name);
            Assert.AreEqual("foobar", products[1].Name);
            Assert.AreEqual("barfoo", products[2].Name);
            Assert.AreEqual("bar", products[3].Name);
        }
    }
}
