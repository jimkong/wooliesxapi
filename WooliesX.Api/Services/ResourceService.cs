﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using WooliesX.Api.Models;

namespace WooliesX.Api.Services
{
    public class WooliesResourceConfig
    {
        public string BaseAddress { get; set; }
        public string Token { get; set; }
    }

    public interface IResourceService
    {
        Task<IEnumerable<Product>> GetProducts();
        Task<IEnumerable<ShopperHistory>> GetShopperHistory();
        Task<int> CalculateTrolleyValue(Trolley trolley);
    }

    public class ResourceService : IResourceService
    {
        private readonly IOptions<WooliesResourceConfig> _configuration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly HttpClient _httpClient;
        private readonly string _token;

        public ResourceService(
            IOptions<WooliesResourceConfig> configuration,
            IHttpClientFactory httpClientFactory)
        {
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;

            _token = _configuration.Value.Token;

            _httpClient = _httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri(_configuration.Value.BaseAddress);
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            var endpoint = 
                $"api/resource/products?token={_token}";

            var response = await _httpClient.GetAsync(endpoint);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsAsync<IEnumerable<Product>>();

            return result;
        }

        public async Task<IEnumerable<ShopperHistory>> GetShopperHistory()
        {
            var endpoint =
                $"api/resource/shopperHistory?token={_token}";

            var response = await _httpClient.GetAsync(endpoint);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsAsync<IEnumerable<ShopperHistory>>();

            return result;
        }

        public async Task<int> CalculateTrolleyValue(Trolley trolley)
        {
            var endpoint =
                $"api/resource/trolleyCalculator?token={_token}";

            var response = await _httpClient.PostAsJsonAsync(endpoint, trolley);

            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsAsync<double>();

            return (int)result;
        }
    }
}
