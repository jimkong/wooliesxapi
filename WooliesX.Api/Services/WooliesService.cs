﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.Api.Enums;
using WooliesX.Api.Models;

namespace WooliesX.Api.Services
{
    public interface IWooliesService
    {
        User GetUserDetails();
        Task<IEnumerable<Product>> GetSortedProducts(SortOption sortOption);
        Task<int> CalculateTrolleyValue(Trolley trolley);
    }

    public class WooliesService : IWooliesService
    {
        private readonly IOptions<User> _configuration;
        private readonly IResourceService _resourceService;

        public WooliesService(
            IOptions<User> configuration,
            IResourceService resourceService)
        {
            _configuration = configuration;
            _resourceService = resourceService;
        }

        public User GetUserDetails()
        {
            return _configuration.Value;
        }

        public async Task<IEnumerable<Product>> GetSortedProducts(SortOption sortOption)
        {
            switch(sortOption)
            {
                case SortOption.Low:
                case SortOption.High:
                case SortOption.Ascending:
                case SortOption.Descending:
                    return await SortProducts(sortOption);
                case SortOption.Recommended:
                    return await SortRecommended();
                default:
                    throw new InvalidOperationException("Invalid sort option given");
            }
        }

        private async Task<IEnumerable<Product>> SortProducts(SortOption sortOption)
        {
            var products = await _resourceService.GetProducts();

            switch(sortOption)
            {
                case SortOption.Low:
                    return products.OrderBy(product => product.Price).ToList();
                case SortOption.High:
                    return products.OrderByDescending(product => product.Price).ToList();
                case SortOption.Ascending:
                    return products.OrderBy(product => product.Name).ToList();
                case SortOption.Descending:
                    return products.OrderByDescending(product => product.Name).ToList();
                default:
                    throw new InvalidOperationException("Invalid sort option given for sorting products");
            }
        }

        private async Task<IEnumerable<Product>> SortRecommended()
        {
            var shopperHistoryTask = _resourceService.GetShopperHistory();
            var productsTask = _resourceService.GetProducts();

            var shopperHistory = await shopperHistoryTask;
            var products = (await productsTask).ToList();

            var productPurchaseCounter = products.ToDictionary(product => product.Name, product => 0);

            foreach (var history in shopperHistory)
            {
                foreach (var product in history.Products)
                {
                    if (!productPurchaseCounter.ContainsKey(product.Name))
                    {
                        productPurchaseCounter[product.Name] = 0;
                        products.Add(product);
                    }

                    productPurchaseCounter[product.Name] += (int)product.Quantity;
                }
            }

            foreach (var product in products)
            {
                product.Quantity = productPurchaseCounter[product.Name];
            }

            return products.OrderByDescending(product => productPurchaseCounter[product.Name]).ToList();
        }

        public async Task<int> CalculateTrolleyValue(Trolley trolley)
        {
            return await _resourceService.CalculateTrolleyValue(trolley);
        }
    }
}
