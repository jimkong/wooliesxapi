﻿namespace WooliesX.Api.Constants
{
    public static class AppSettingKeys
    {
        public const string UserDetailsSection = "UserDetails";
        public const string WooliesResourcesSection = "WooliesResources";
        public const string BaseAddress = "BaseAddress";
        public const string Token = "Token";
    }
}
