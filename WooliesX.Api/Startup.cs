﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WooliesX.Api.Constants;
using WooliesX.Api.Models;
using WooliesX.Api.Services;

namespace WooliesX.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpClient();

            services.AddHttpClient<ResourceService>();

            services.AddScoped<IWooliesService, WooliesService>();
            services.AddScoped<IResourceService, ResourceService>();

            services.Configure<User>(Configuration.GetSection(AppSettingKeys.UserDetailsSection));
            services.Configure<WooliesResourceConfig>(config =>
            {
                config.BaseAddress = Configuration.GetSection(AppSettingKeys.WooliesResourcesSection)[AppSettingKeys.BaseAddress];
                config.Token = Configuration.GetSection(AppSettingKeys.UserDetailsSection)[AppSettingKeys.Token];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
