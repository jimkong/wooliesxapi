﻿using System.Collections.Generic;

namespace WooliesX.Api.Models
{
    public class Trolley
    {
        public IEnumerable<TrolleyProduct> Products { get; set; }
        public IEnumerable<Special> Specials { get; set; }
        public IEnumerable<TrolleyQuantity> Quantities { get; set; }
    }

    public class TrolleyProduct
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }

    public class Special
    {
        public IEnumerable<TrolleyQuantity> Quantities { get; set; }
        public double Total { get; set; }
    }

    public class TrolleyQuantity
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
