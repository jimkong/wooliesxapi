﻿namespace WooliesX.Api.Models
{
    public class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public double Quantity { get; set; } // Why does the API return a double for this?
    }
}
