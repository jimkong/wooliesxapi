﻿namespace WooliesX.Api.Enums
{
    public enum SortOption
    {
        Low,
        High,
        Ascending,
        Descending,
        Recommended
    }
}
