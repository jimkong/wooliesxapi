﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.Api.Enums;
using WooliesX.Api.Models;
using WooliesX.Api.Services;

namespace WooliesX.Api.Controllers
{
    [Route("api/answers/")]
    [ApiController]
    public class WooliesController : ControllerBase
    {
        private readonly IWooliesService _wooliesService;

        public WooliesController(IWooliesService wooliesService)
        {
            _wooliesService = wooliesService;
        }

        [Route("user")]
        [HttpGet]
        public ActionResult<User> GetUser()
        {
            var response = _wooliesService.GetUserDetails();

            return Ok(response);
        }

        [Route("sort")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetSortedProducts(string sortOption)
        {
            if (!Enum.TryParse(sortOption, true, out SortOption selectedSortOption))
            {
                return BadRequest();
            }

            var response = await _wooliesService.GetSortedProducts(selectedSortOption);

            return Ok(response);
        }

        [Route("trolleyCalculator")]
        [HttpPost]
        public async Task<ActionResult<int>> CalculateTrolleyValue([FromBody] Trolley trolley)
        {
            var response = await _wooliesService.CalculateTrolleyValue(trolley);

            return Ok(response);
        }
    }
}
